<?php
class jeux_model extends Model 
{

    function jeux_model()
    {
        parent::Model();
        $this->load->database();
    }
    
	function get_list_etat()
	{
		//select * from 'etat'
		$query = $this->db->get('etat');
		
		return $query->result();
	}
	
	function search_prix_mini_mm_etat($etat, $plancher)
	{
		//select min('prix_vente') from 'concurrence' where 'etat_produit' = $etat and 'prix_vente' > $plancher +0.01
		$this->db->select_min('prix_vente');
		$this->db->where('etat_produit', $etat);
		$this->db->where('prix_vente >', $plancher + 0.01);
		$query = $this->db->get('concurrence');
		if ($query->num_rows() > 0)
		{
			return $query->row()->prix_vente;
		}
		else 
		{
			return 0;
		}
	}
	
	function search_prix_mini_meilleur_etat($etat, $plancher)
	{
		//select min('prix_vente') from 'concurrence' where 'etat_produit' < $etat and 'prix_vente' > $plancher +1
		$this->db->select_min('prix_vente');
		$this->db->where('etat_produit <', $etat);
		$this->db->where('prix_vente >', $plancher + 1);
		$query = $this->db->get('concurrence');
		
		if ($query->num_rows() > 0)
		{
			return $query->row()->prix_vente;
		}
		else 
		{
			return 0;
		}
	}
	
	
}
?>