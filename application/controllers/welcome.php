<?php

class welcome extends Controller 
{

	function welcome()
	{
		parent::Controller();	
		$this->load->model('jeux_model');
	}
	
	function index()
	{
		//chargement des états des produits pour alimenter la liste déroulante 
		$data['etat'] = $this->jeux_model->get_list_etat();
		$this->load->view('welcome_view',$data);
	}
	
	function search()
	{
		$_etat = $this->input->post('etatProduit');
		$_plancher = $this->input->post('prixPlancher');
		
		//on récupère le plus bas prix chez la concurrence pour le même état
		$_query_mm_etat = $this->jeux_model->search_prix_mini_mm_etat($_etat, $_plancher);
		
		if ($_query_mm_etat == 0)
		{
			//on ne trouve rien, on renvoie 0
			$data['prix_vente'] = 0;
		}
		else
		{
			if ($_etat != 1) //l'article est d'occasion
			{
				//on récupère le plus bas prix chez la concurrence pour un meilleur etat
				$_query_meilleur_etat = $this->jeux_model->search_prix_mini_meilleur_etat($_etat, $_plancher);
				$data['prix_vente'] = $this->compare_prix($_query_mm_etat,$_query_meilleur_etat);
			}
			else //l'article est neuf, il ne peut pas y avoir de meilleur état
			{
				$data['prix_vente'] = $_query_mm_etat - 0.01;
			}	
		}
		$this->load->view('result_view',$data);		
	}
	
	function compare_prix($prix_mini_vente_mm_etat, $prix_mini_vente_meilleur_etat)
	{
		$prix_mini_vente_mm_etat -= 0.01;
		$prix_mini_vente_meilleur_etat -= 1;
		
		//on retourne le plus bas prix pour toujours satisfaire l'une des deux conditions
		if ($prix_mini_vente_mm_etat < $prix_mini_vente_meilleur_etat)
		{
			return $prix_mini_vente_mm_etat;
		}
		else
		{
			return $prix_mini_vente_meilleur_etat;
		}	
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */