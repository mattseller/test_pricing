<html>
<head>
<title>R&eacute;sultat</title>

</head>
<body>
<?php
if ($prix_vente != 0)
{
	echo htmlentities('Cet objet sera mis en vente au prix de : ' . $prix_vente . '€', ENT_QUOTES, 'UTF-8');
}
else
{
	echo htmlentities('Aucun article n\'a été trouvé dans la concurrence au dessus de ce prix plancher.', ENT_QUOTES, 'UTF-8');
}
?>
<br />
<a href="welcome">Retour</a>
</body>
</html>