<html>
<head>
<title>Choisir un objet &agrave; vendre</title>

</head>
<body>
<?php
$this->load->helper('form');
$this->load->helper('html');
echo form_open('welcome/search');
$data = array('name'        => 'prixPlancher',
              'id'          => 'prixPlancher',
              'maxlength'   => '20',
              'size'        => '20',
              );
echo form_label(htmlentities('Définir un prix plancher : ', ENT_QUOTES, 'UTF-8'), 'prixPlancher');
echo form_input($data);

echo br(1);
echo form_label(htmlentities('Choisir l\'état du produit : ', ENT_QUOTES, 'UTF-8'), 'etatProduit');
?>
<select name="etatProduit">
<?php 
foreach($etat as $row)
{
	echo '<option value="' . $row->etat_id . '">' . htmlentities($row->etat_label, ENT_QUOTES, 'UTF-8'). '</option>';
}
?>
</select>
<br />
<?php
echo form_submit('submit', 'Envoyer');
echo form_close();

 ?>
 
</body>
</html>